//
//  File.swift
//  Arcane
//
//  Created by Dmitry on 12.02.2018.
//

import Foundation
import Sodium
import CoreBitcoin
import ethers
import CryptoSwift
import AFNetworking

fileprivate class BlockchainAPI {

    typealias BlockchainAPICompletition = (Any?, Error?) -> Void

    private static let baseURLString = "https://blockchain.info"
    static let shared = BlockchainAPI.init()

    func GETPath(_ path:String, withParams params:[String:Any]?, completition:@escaping BlockchainAPICompletition) {

        let manager = AFHTTPSessionManager.init(sessionConfiguration: URLSessionConfiguration.default)!

        manager.get(BlockchainAPI.baseURLString + path, parameters: params, success: { (task, result) in
            completition(result, nil)
        }) { (task, error) in
            completition(nil, error)
        }
    }
}


class BTCService: WalletService, WalletServiceInternal {

    private var transactionBuilder:BitcoinTransactionBuilder {
        return BitcoinTransactionBuilder.init()
    }

    static var walletType: WalletType {
        return .BTC
    }

    let storage: WalletStorage
    let passphraseHash: SecureData

    required init(storage: WalletStorage, passphrase: String) {
        self.storage = storage
        self.passphraseHash = SecureData.create(from: EthereumService.calculatePassphraseHash(passphrase))
    }

    func isValidMnemonic(_ phrase: String) -> Bool {
        return Account.isValidMnemonicPhrase(phrase)
    }

    func isValidMnemonic(word: String) -> Bool {
        return Account.isValidMnemonicWord(word)
    }

    func isValidPrivateKey(_ key: String) -> Bool {
        switch key.count {
        case 64:
            if let bk = BTCKey(privateKey: Data(hex: key)), let _ = bk.privateKey {
                return true
            } else {
                return false
            }
        case 51...52:
            if let bk = BTCKey(wif: key), let _ = bk.privateKey {
                return true
            } else {
                return false
            }
        default:
            return false
        }
    }

    func isValidPrivateKey(_ key: SecureData) -> Bool {
        switch key.length {
        case 32:
            if let bk = BTCKey(privateKey: (key as Data)), let _ = bk.privateKey {
                return true
            } else {
                return false
            }
        default:
            return false
        }
    }

    func isValidAddress(_ address: String) -> Bool {
        guard let _ = OktosWalletKit.isTestnet ? BTCPublicKeyAddressTestnet(string: address) : BTCPublicKeyAddress(string: address) else {
            return false
        }
        return true
    }

    func getBalance(for wallet: WalletProtocol, callback: @escaping (WalletValue) -> ()) {

        self.getBalanceForWallet(with: GeneralWalletId.init(id:wallet.address.userRepresentation, type:WalletType.BTC), callback: callback)
    }

    func getBalanceForWallet(with id: WalletId, callback: @escaping (WalletValue) -> ()) {

        BlockchainAPI.shared.GETPath("/balance", withParams: ["active" : id.id]) { (result, error) in

            guard error == nil else {

                callback(GeneralWalletValue(userRepresentation: "0", value: 0))
                return
            }

            var balance:Int64 = 0

            if let dictionary = (result as? [String:Any])?.first?.value as? [String:Any]{

                if let balanceInSatoshis = dictionary["final_balance"] as? Int64 {
                    balance = balanceInSatoshis
                }
            }

            callback(GeneralWalletValue(userRepresentation: "\(Float(balance) / 100_000_000.0) BTC", value: Decimal.init(balance)))
        }
    }


    func sendPayment(from wallet: WalletProtocol, to address: String, value: Decimal, callback: @escaping (Bool) -> ()) throws {
        guard let privateKey = wallet.privateKey?.data else { throw WalletServiceError.noPrivateKey }

        self.transactionBuilder.buildTransaction(amount: ((value * 100_000_000.0) as NSDecimalNumber).int64Value,
                                                 receiverAddress: address,
                                                 privateKey: privateKey as Data,
                                                 fee: 0) {
            (data, error) in

            guard error == nil, data != nil else {
                callback(false)
                return
            }

            let request = BTCBlockchainInfo.init().requestForTransactionBroadcast(with: data)! as URLRequest

            NSURLConnection.sendAsynchronousRequest(request, queue: OperationQueue.init(), completionHandler: {
                (response, data, error) in

                DispatchQueue.main.async {
                    callback(error == nil)
                }
            })
        }
    }

    func getTransactions(for wallet: WalletProtocol, callback: @escaping ([WalletTransaction]) -> ()) {

        BlockchainAPI.shared.GETPath("/rawaddr/\(wallet.address)", withParams: nil) { (result, error) in

//            guard error == nil else {
//
//                completition(nil, error)
//                return
//            }
//
//            if let dictionary = result as? [String:Any], let transactionsArray = dictionary["txs"] as? [[String:Any]] {
//
//                var transactions = [Transaction]()
//
//                transactionsArray.forEach({ (dictionary) in
//
//                    if let transaction = BlockChainInfoTransaction.init(withDictionary: dictionary) {
//
//                        transactions.append(transaction)
//                    }
//                })
//
//                completition(transactions, nil)
//            }
//            else {
//                //TODO: Generate error
//            }
        }
    }

    func transactionInfo(from data: Data) throws -> WalletTransactionInfo {
        guard let transaction = BTCTransaction(data: data) else { throw WalletServiceError.failedToDecodeTransaction }

        let outputs: [(String, WalletValue)] = transaction.outputs.compactMap({ $0 as? BTCTransactionOutput })
                                                                  .map { ($0.script.hex, Decimal($0.value)) }
                                                                  .map { ($0.0, GeneralWalletValue(userRepresentation: "\($0.1)", value: $0.1)) }

        let fee = Decimal(transaction.fee)

        return WalletTransactionInfo(outputs: Dictionary(uniqueKeysWithValues: outputs),
                                     fee: GeneralWalletValue(userRepresentation: "\(fee)", value: fee))
    }

    var sodium: Sodium = Sodium()

    func createNewWalletData() -> SecureData {

        let entropy = BTCRandomDataWithLength(16) as Data

        let mnemonic = BTCMnemonic.init(entropy: entropy, password: nil, wordListType: .english)!

        let coin = OktosWalletKit.isTestnet ? 1 : 0

        let key = mnemonic.keychain.key(withPath: "m/44'/\(coin)'/0'/0/0")!

        let info = GeneralWalletInfo(address: OktosWalletKit.isTestnet ? key.addressTestnet.data.secure : key.compressedPublicKeyAddress.data.secure,
                                     privateKey: (key.privateKey as Data).secure,
                                     mnemonic: mnemonic.data.secure)

        let secure = SecureData.create(from: try! JSONEncoder().encode(info))

        return secure
    }
}

fileprivate class BitcoinTransactionBuilderDataSource:NSObject, BTCTransactionBuilderDataSource {

    private var privateKey:Data
    private var unspentOutputs:[BTCTransactionOutput]

    init(privateKey:Data, unspentOutputs:[BTCTransactionOutput]) {
        self.privateKey = privateKey
        self.unspentOutputs = unspentOutputs
    }

    func unspentOutputs(for txbuilder: BTCTransactionBuilder!) -> NSEnumerator! {
        return NSArray.init(array: self.unspentOutputs).objectEnumerator()
    }

    func transactionBuilder(_ txbuilder: BTCTransactionBuilder!, keyForUnspentOutput txout: BTCTransactionOutput!) -> BTCKey! {
        return BTCKey.init(privateKey: self.privateKey)
    }
}

fileprivate class BitcoinTransactionBuilder:NSObject  {

    lazy private var operationQueue = OperationQueue.init()

    func buildTransaction(amount:Int64, receiverAddress:String, privateKey:Data, fee:Int64, completition: @escaping (Data?, Error?) -> Void)
    {
        self.operationQueue.addOperation {

            var data:Data?

            do {
                data = try self.buildTransactionSync(amount: amount,
                                                     receiverAddress: receiverAddress,
                                                     privateKey: privateKey,
                                                     fee: fee)
            }
            catch {
                completition(nil, error)
                return
            }

            completition(data, nil)
        }
    }

    private func buildTransactionSync(amount:Int64, receiverAddress:String, privateKey:Data, fee:Int64) throws -> Data? {

        let key = BTCKey.init(privateKey: privateKey)!

        guard let senderAddress = key.compressedPublicKeyAddress else {
            return nil
        }

        let unspentOutputs = try BTCBlockchainInfo.init().unspentOutputs(withAddresses: [senderAddress]) as! [BTCTransactionOutput]

        let dataSource = BitcoinTransactionBuilderDataSource.init(privateKey: privateKey, unspentOutputs: unspentOutputs)

        let builder = BTCTransactionBuilder.init()
        builder.dataSource = dataSource
        builder.outputs = [BTCTransactionOutput.init(value: amount, address: BTCAddress.init(string: receiverAddress))!]
        builder.changeAddress = BTCAddress.init(string: senderAddress.string)

        return try builder.buildTransaction().transaction.data
    }
}

