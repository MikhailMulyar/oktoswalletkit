//
//  BTCWallet.swift
//  Arcane
//
//  Created by Dmitry on 12.02.2018.
//

import Foundation
import CoreBitcoin
import CryptoSwift


class BTCWallet: WalletInternal {

    typealias WalletData = WalletDataProtocol

    var data: WalletDataProtocol

    func walletData() -> BTCWallet.WalletData {
        return self.data
    }

    required init?(mnemonic: String, password: String? = nil, keyPath: WalletKeyPath?) {

        guard let mnemonicObject = BTCMnemonic.init(words: mnemonic.components(separatedBy: " "), password: password, wordListType: .english),
              let key = mnemonicObject.keychain.key(withPath: (keyPath ?? (OktosWalletKit.isTestnet ? WalletKeyPath.BitcoinTestnet : WalletKeyPath.Bitcoin)).path) else {
            return nil
        }

        let info = GeneralWalletInfo(address: OktosWalletKit.isTestnet ? key.addressTestnet.data.secure : key.compressedPublicKeyAddress.data.secure,
                                     privateKey: (key.privateKey as Data).secure,
                                     mnemonic: mnemonicObject.data.secure)

        self.data = BTCInfo.init(genericInfo: info)
    }

    required init?(walletData: SecureData) {

        guard let data = BTCInfo.init(secureData: walletData) else {

            return nil
        }

        self.data = data
    }

    required init?(privateKey: SecureData) {

        let k: BTCKey?

        switch privateKey.length {
        case 32:
            k = BTCKey.init(privateKey: privateKey as Data)
        default:
            return nil
        }

        guard let key = k else {
            return nil
        }

        let info = GeneralWalletInfo(address: OktosWalletKit.isTestnet ? key.addressTestnet.data.secure : key.compressedPublicKeyAddress.data.secure,
                                     privateKey: (key.privateKey as Data).secure,
                                     mnemonic: nil)

        self.data = BTCInfo.init(genericInfo: info)
    }

    required init?(privateKey: String) {
        let k: BTCKey?

        switch privateKey.count {
        case 64:
            k = BTCKey.init(privateKey: Data(hex: privateKey))
        default:
            k = BTCKey(wif: privateKey)
        }

        guard let key = k else {
            return nil
        }

        let info = GeneralWalletInfo(address: OktosWalletKit.isTestnet ? key.addressTestnet.data.secure : key.compressedPublicKeyAddress.data.secure,
                                     privateKey: (key.privateKey as Data).secure,
                                     mnemonic: nil)

        self.data = BTCInfo.init(genericInfo: info)
    }

    required init?(address: SecureData) {

        let addressString = String.init(data: address as Data, encoding: String.Encoding.utf8)

        let address = OktosWalletKit.isTestnet ? BTCPublicKeyAddressTestnet.init(string: addressString) : BTCPublicKeyAddress.init(string: addressString)

        guard address != nil else {
            return nil
        }

        let info = GeneralWalletInfo(address: address!.data.secure, privateKey: nil, mnemonic: nil)
        self.data = BTCInfo(genericInfo: info)
    }

    func sign(data: Data) -> Data? {
        guard let privateKey = self.walletData().privateKey,
              let key = BTCKey.init(privateKey: privateKey.data as Data) else { return nil }

        let signed = key.signature(forHash: data)
        return signed
    }
}


struct BTCInfo: WalletDataProtocol {

    let genericInfo: GeneralWalletInfo
    var secureData: SecureData

    init(genericInfo: GeneralWalletInfo) {

        self.genericInfo = genericInfo
        self.secureData = SecureData.create(from: try! JSONEncoder().encode(genericInfo))
    }

    init?(secureData: SecureData) {

        guard let info = try? JSONDecoder().decode(GeneralWalletInfo.self, from: secureData as Data) else { return nil }

        self.secureData = secureData
        self.genericInfo = info
    }

    var id: WalletId {
        return GeneralWalletId(id: self.address.userRepresentation, type: .BTC)
    }

    var privateKey: WalletPrivateKey? {
        guard let privateKey = self.genericInfo.privateKey,
              let key = BTCKey.init(privateKey: privateKey as Data) else { return nil }
        return GeneralWalletKey(userRepresentation: key.wif, internalData: privateKey)
    }

    var publicKey: WalletPublicKey? {
        guard let privateKey = self.genericInfo.privateKey,
              let key = BTCKey.init(privateKey: privateKey as Data) else { return nil }
        let data = key.compressedPublicKey as Data
        return GeneralWalletKey(userRepresentation: data.toHexString(), internalData: data.secure)
    }

    var address: WalletAddress {
        let address = OktosWalletKit.isTestnet ? BTCPublicKeyAddressTestnet(data: self.genericInfo.address as Data)! : BTCPublicKeyAddress(data: self.genericInfo.address as Data)!

        return GeneralWalletAddress(userRepresentation: address.string, internalData: self.genericInfo.address)
    }

    var mnemonic: WalletMnemonic? {

        let mnemonic = BTCMnemonic.init(data: self.genericInfo.mnemonic as Data?)

        guard let data = mnemonic?.data, let phrase = (mnemonic?.words as? [String])?.joined(separator: " ") else {
            return nil
        }

        return GeneralWalletMnemonic(userRepresentation: phrase, internalData: data.secure)
    }
}
