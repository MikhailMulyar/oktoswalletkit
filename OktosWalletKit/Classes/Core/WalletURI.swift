//
// Created by Mikhail Mulyar on 06/04/2018.
//

import Foundation


extension WalletType {
    var uriScheme: String {
        switch self {
        case .BTC:
            return "bitcoin"
        case .ETH:
            return "ethereum"
        }
    }

    init?(scheme: String) {
        switch scheme {
        case "bitcoin":
            self = .BTC
        case "ethereum":
            self = .ETH
        default:
            return nil
        }
    }
}


public struct WalletURI {

    enum Key: String {
        case amount
        case label
        case message
    }


    public let type: WalletType
    public let address: String
    public let amount: Decimal?
    public let label: String?
    public let message: String?
    public let parameters: [String: String?]?

    public var url: URL? {

        var queryItems = [URLQueryItem]()
        if let amount = amount { queryItems.append(URLQueryItem(name: Key.amount.rawValue, value: "\(amount)")) }
        if let label = label { queryItems.append(URLQueryItem(name: Key.label.rawValue, value: label)) }
        if let message = message { queryItems.append(URLQueryItem(name: Key.message.rawValue, value: message)) }

        parameters?.forEach { key, value in queryItems.append(URLQueryItem(name: key, value: value)) }

        var components = URLComponents()
        components.scheme = self.type.uriScheme
        components.path = self.address

        if queryItems.isEmpty == false {
            components.queryItems = queryItems
        }

        return components.url
    }

    public init(type: WalletType,
                address: String,
                amount: Decimal? = nil,
                label: String? = nil,
                message: String? = nil,
                parameters: [String: String]? = nil) {
        self.type = type
        self.address = address
        self.amount = amount
        self.label = label
        self.message = message
        self.parameters = parameters

    }

    public init?(url: URL) {
        guard let components = URLComponents(url: url, resolvingAgainstBaseURL: true),
              let scheme = components.scheme,
              let type = WalletType(scheme: scheme) else { return nil }

        var queryItems = components.queryItems?.reduce(into: [String: URLQueryItem]()) { $0[$1.name] = $1 }

        self.type = type
        self.address = components.path
        self.amount = queryItems?[Key.amount.rawValue]?.value.flatMap { Decimal(string: $0) }
        self.label = queryItems?[Key.label.rawValue]?.value
        self.message = queryItems?[Key.message.rawValue]?.value

        queryItems?[Key.amount.rawValue] = nil
        queryItems?[Key.label.rawValue] = nil
        queryItems?[Key.message.rawValue] = nil

        var params = [String: String?]()
        queryItems?.values.forEach {
            item in
            params[item.name] = item.value
        }

        if params.isEmpty {
            self.parameters = nil
        } else {
            self.parameters = params
        }
    }
}