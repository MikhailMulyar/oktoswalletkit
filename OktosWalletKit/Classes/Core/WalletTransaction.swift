//
// Created by Mikhail Mulyar on 08/05/2018.
//

import Foundation


public protocol WalletTransaction {
    var to: WalletAddress { get }
    var from: WalletAddress { get }
    var value: WalletValue { get }
}


public struct WalletTransactionInfo {
    public let outputs: [String: WalletValue]
    public let fee: WalletValue
}