//
// Created by Mikhail Mulyar on 26/01/2018.
//

import Foundation
import ethers
import Sodium


protocol WalletServiceInternal {
    var sodium: Sodium { get }
    var storage: WalletStorage { get }
    var passphraseHash: SecureData { get }
    func createNewWalletData() -> SecureData
}


extension WalletService {

    public func wallet(for id: WalletId) -> WalletProtocol? {
        guard let s = self as? WalletServiceInternal else {
            fatalError("Service should implement WalletServiceInternal protocol")
        }
        guard let data = s.storage.getWalletData(for: id) else { return nil }

        guard let unencrypted = s.sodium.secretBox.open(nonceAndAuthenticatedCipherText: .init(data),
                                                        secretKey: .init(s.passphraseHash)) else { return nil }

        return GeneralWallet(type: Self.walletType, walletData: SecureData.create(from: unencrypted))
    }

    public func wallet(with data: SecureData) -> WalletProtocol? {
        guard let _ = self as? WalletServiceInternal else {
            fatalError("Service should implement WalletServiceInternal protocol")
        }

        guard let wallet = GeneralWallet(type: Self.walletType, walletData: data) else {
            return nil
        }

        return wallet
    }

    public func createNewWallet() -> WalletProtocol {
        guard let s = self as? WalletServiceInternal else {
            fatalError("Service should implement WalletServiceInternal protocol")
        }
        let newWalletData: SecureData = s.createNewWalletData()
        guard let newWallet = GeneralWallet(type: Self.walletType, walletData: newWalletData) else {
            fatalError("Service failed to create wallet")
        }
        guard let encrypted: Bytes = s.sodium.secretBox.seal(message: .init(newWalletData), secretKey: .init(s.passphraseHash)) else {
            fatalError("Service failed to encrypt wallet")
        }

        s.storage.storeWalletData(SecureData.create(from: encrypted), for: newWallet.id)

        return newWallet
    }

    public var wallets: [WalletProtocol] {
        guard let s = self as? WalletServiceInternal else {
            fatalError("Service should implement WalletServiceInternal protocol")
        }
        return s.storage.wallets(of: Self.walletType).compactMap {
            guard let unencrypted = s.sodium.secretBox.open(nonceAndAuthenticatedCipherText: .init($0),
                                                            secretKey: .init(s.passphraseHash)) else { return nil }
            return GeneralWallet(type: Self.walletType, walletData: SecureData.create(from: unencrypted))
        }
    }

    public func importWallet(mnemonic: String) throws -> WalletProtocol {
        guard let s = self as? WalletServiceInternal else {
            fatalError("Service should implement WalletServiceInternal protocol")
        }

        guard let newWallet = GeneralWallet(type: Self.walletType, mnemonic: mnemonic) else {
            throw WalletServiceError.incorrectMnemonic
        }

        if let _ = self.wallet(for: newWallet.id) {
            throw WalletServiceError.walletAlreadyExists
        }

        guard let encrypted: Bytes = s.sodium.secretBox.seal(message: .init(newWallet.data), secretKey: .init(s.passphraseHash)) else {
            fatalError("Service failed to encrypt wallet")
        }

        s.storage.storeWalletData(SecureData.create(from: encrypted), for: newWallet.id)

        return newWallet
    }

    public func importWallet(mnemonic: String, password: String?, keyPath: WalletKeyPath?) throws -> WalletProtocol {
        guard let s = self as? WalletServiceInternal else {
            fatalError("Service should implement WalletServiceInternal protocol")
        }

        guard let newWallet = GeneralWallet(type: Self.walletType, mnemonic: mnemonic, password: password, keyPath: keyPath) else {
            throw WalletServiceError.incorrectMnemonic
        }

        if let _ = self.wallet(for: newWallet.id) {
            throw WalletServiceError.walletAlreadyExists
        }

        guard let encrypted: Bytes = s.sodium.secretBox.seal(message: .init(newWallet.data), secretKey: .init(s.passphraseHash)) else {
            fatalError("Service failed to encrypt wallet")
        }

        s.storage.storeWalletData(SecureData.create(from: encrypted), for: newWallet.id)

        return newWallet
    }

    public func importWallet(privateKey: SecureData) throws -> WalletProtocol {
        guard let s = self as? WalletServiceInternal else {
            fatalError("Service should implement WalletServiceInternal protocol")
        }

        guard let newWallet = GeneralWallet(type: Self.walletType, privateKey: privateKey) else {
            throw WalletServiceError.incorrectPrivateKey
        }

        if let _ = self.wallet(for: newWallet.id) {
            throw WalletServiceError.walletAlreadyExists
        }

        guard let encrypted: Bytes = s.sodium.secretBox.seal(message: .init(newWallet.data), secretKey: .init(s.passphraseHash)) else {
            throw WalletServiceError.failedToEncryptData
        }

        s.storage.storeWalletData(SecureData.create(from: encrypted), for: newWallet.id)

        return newWallet
    }

    public func importWallet(privateKey: String) throws -> WalletProtocol {
        guard let s = self as? WalletServiceInternal else {
            fatalError("Service should implement WalletServiceInternal protocol")
        }

        guard let newWallet = GeneralWallet(type: Self.walletType, privateKey: privateKey) else {
            throw WalletServiceError.incorrectPrivateKey
        }

        if let _ = self.wallet(for: newWallet.id) {
            throw WalletServiceError.walletAlreadyExists
        }

        guard let encrypted: Bytes = s.sodium.secretBox.seal(message: .init(newWallet.data), secretKey: .init(s.passphraseHash)) else {
            throw WalletServiceError.failedToEncryptData
        }

        s.storage.storeWalletData(SecureData.create(from: encrypted), for: newWallet.id)

        return newWallet
    }

    public func importWallet(address: SecureData) throws -> WalletProtocol {
        guard let s = self as? WalletServiceInternal else {
            fatalError("Service should implement WalletServiceInternal protocol")
        }

        guard let newWallet = GeneralWallet(type: Self.walletType, address: address) else {
            throw WalletServiceError.incorrectAddress
        }

        if let _ = self.wallet(for: newWallet.id) {
            throw WalletServiceError.walletAlreadyExists
        }

        guard let encrypted: Bytes = s.sodium.secretBox.seal(message: .init(newWallet.data), secretKey: .init(s.passphraseHash)) else {
            throw WalletServiceError.failedToEncryptData
        }

        s.storage.storeWalletData(SecureData.create(from: encrypted), for: newWallet.id)

        return newWallet
    }

    public func remove(wallet: WalletProtocol) throws {
        guard let s = self as? WalletServiceInternal else {
            fatalError("Service should implement WalletServiceInternal protocol")
        }

        guard let _ = s.storage.getWalletData(for: wallet.id) else {
            throw WalletServiceError.walletDoesNotExist
        }

        s.storage.removeWalletData(for: wallet.id)
    }

    public func removeWallet(with id: WalletId) throws {
        guard let s = self as? WalletServiceInternal else {
            fatalError("Service should implement WalletServiceInternal protocol")
        }

        guard let _ = s.storage.getWalletData(for: id) else {
            throw WalletServiceError.walletDoesNotExist
        }

        s.storage.removeWalletData(for: id)
    }

    public func update(privateKey: SecureData, for wallet: WalletProtocol) throws -> WalletProtocol {
        guard let s = self as? WalletServiceInternal else {
            fatalError("Service should implement WalletServiceInternal protocol")
        }

        guard let _ = s.storage.getWalletData(for: wallet.id) else {
            throw WalletServiceError.walletDoesNotExist
        }

        guard let newWallet = GeneralWallet(type: wallet.id.type, privateKey: privateKey), newWallet.id.id == wallet.id.id else {
            throw WalletServiceError.incorrectPrivateKey
        }

        guard let encrypted: Bytes = s.sodium.secretBox.seal(message: .init(newWallet.data), secretKey: .init(s.passphraseHash)) else {
            throw WalletServiceError.failedToEncryptData
        }

        s.storage.storeWalletData(SecureData.create(from: encrypted), for: newWallet.id)

        return newWallet
    }
}


extension WalletServiceInternal {
    static func calculatePassphraseHash(_ passphrase: String) -> Data {
        guard let data = "deterministicsaltrandombytevalue".data(using: .utf8),
              let salt = Sodium().randomBytes.deterministic(length: 16, seed: Array(data)) else { return Data() }
        let saltData = Data(bytes: salt)
        return (passphrase as NSString).passwordHash(withSalt: saltData)
    }
}
