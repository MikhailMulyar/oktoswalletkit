//
// Created by Mikhail Mulyar on 17/04/2018.
//

import Foundation


//BIP44
public struct WalletKeyPath {
    public let purpose: Int?
    public let coinType: Int?
    public let account: Int?
    public let change: Int?
    public let index: Int?

    public init(purpose: Int? = nil, coinType: Int? = nil, account: Int? = nil, change: Int? = nil, index: Int? = nil) {
        self.purpose = purpose
        self.coinType = coinType
        self.account = account
        self.change = change
        self.index = index
    }

    public init?(path: String) {

        var p = path

        if path == "m" || path == "/" || path == "" {
            return nil
        } else {
            if p.hasPrefix("m/"), let r = p.range(of: "m/") {
                p.replaceSubrange(r, with: "")
            }
            var purpose: Int? = nil
            var coinType: Int? = nil
            var account: Int? = nil
            var change: Int? = nil
            var index: Int? = nil
            for (i, chunk) in p.components(separatedBy: "/").enumerated() {
                let max: Int64 = 0x80000000
                switch i {
                case 0:
                    if let val = Int64(chunk), max > Int64(val) {
                        purpose = Int(exactly: val)
                    } else {
                        return nil
                    }
                case 1:
                    if let val = Int64(chunk), max > Int64(val) {
                        coinType = Int(exactly: val)
                    } else {
                        return nil
                    }
                case 2:
                    if let val = Int64(chunk), max > Int64(val) {
                        account = Int(exactly: val)
                    } else {
                        return nil
                    }
                case 3:
                    if let val = Int64(chunk), max > Int64(val) {
                        change = Int(exactly: val)
                    } else {
                        return nil
                    }
                case 4:
                    if let val = Int64(chunk), max > Int64(val) {
                        index = Int(exactly: val)
                    } else {
                        return nil
                    }
                default:
                    return nil
                }
            }

            self.init(purpose: purpose, coinType: coinType, account: account, change: change, index: index)
        }
    }

    public static var Bitcoin: WalletKeyPath {
        return WalletKeyPath()
    }
    public static var BitcoinTestnet: WalletKeyPath {
        return WalletKeyPath(purpose: 44, coinType: 1, account: 0, change: 0, index: 0)
    }
    public static var Ethereum: WalletKeyPath {
        return WalletKeyPath(purpose: 44, coinType: 60, account: 0, change: 0, index: 0)
    }

    public var path: String {
        var path = "m"

        guard let p = purpose else { return path }
        path += "/\(p)'"
        guard let t = coinType else { return path }
        path += "/\(t)'"
        guard let a = account else { return path }
        path += "/\(a)'"
        guard let c = change else { return path }
        path += "/\(c)"
        guard let i = index else { return path }
        path += "/\(i)"

        return path
    }
}