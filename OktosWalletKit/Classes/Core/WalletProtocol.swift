//
// Created by Mikhail Mulyar on 26/01/2018.
//

import Foundation


public protocol WalletPrivateKey {
    var userRepresentation: String { get }
    var data: SecureData { get }
}

public protocol WalletPublicKey {
    var userRepresentation: String { get }
    var data: SecureData { get }
}


public protocol WalletMnemonic {
    var userRepresentation: String { get }
    var data: SecureData? { get }
}


public protocol WalletAddress {
    var userRepresentation: String { get }
    var data: SecureData { get }
}


public protocol WalletValue {
    var userRepresentation: String { get }
    var value: Decimal { get }
}


public protocol WalletId {
    var id: String { get }
    var type: WalletType { get }
}

public protocol WalletProtocol {
    var id: WalletId { get }
    var data: SecureData { get }
    var address: WalletAddress { get }
    var privateKey: WalletPrivateKey? { get }
    var publicKey: WalletPublicKey? { get }
    var mnemonic: WalletMnemonic? { get }

    func sign(data: Data) -> Data?
}
