import Foundation


public enum WalletType {
    case BTC
    case ETH
}


public enum WalletServiceError: Error {
    case walletDoesNotExist
    case noPrivateKey
    case incorrectMnemonic
    case incorrectAddress
    case walletAlreadyExists
    case incorrectPrivateKey
    case failedToEncryptData
    case failedToDecodeTransaction
}


public protocol WalletStorage {
    func wallets(of type: WalletType) -> [SecureData]
    func storeWalletData(_ data: SecureData, for id: WalletId)
    func removeWalletData(for id: WalletId)
    func getWalletData(for id: WalletId) -> SecureData?
}


public protocol WalletService {
    init(storage: WalletStorage, passphrase: String)

    static var walletType: WalletType { get }

    func isValidMnemonic(_ phrase: String) -> Bool
    func isValidMnemonic(word: String) -> Bool

    func isValidPrivateKey(_ key: String) -> Bool
    func isValidPrivateKey(_ key: SecureData) -> Bool

    func isValidAddress(_ address: String) -> Bool

    var wallets: [WalletProtocol] { get }

    func wallet(for id: WalletId) -> WalletProtocol?
    func wallet(with data: SecureData) -> WalletProtocol?
    func createNewWallet() -> WalletProtocol

    func importWallet(mnemonic: String) throws -> WalletProtocol
    func importWallet(mnemonic: String, password: String?, keyPath: WalletKeyPath?) throws -> WalletProtocol
    func importWallet(privateKey: SecureData) throws -> WalletProtocol
    func importWallet(privateKey: String) throws -> WalletProtocol
    func importWallet(address: SecureData) throws -> WalletProtocol
    func remove(wallet: WalletProtocol) throws
    func removeWallet(with id: WalletId) throws

    func update(privateKey: SecureData, for wallet: WalletProtocol) throws -> WalletProtocol

    func getBalance(for wallet: WalletProtocol, callback: @escaping (WalletValue) -> ())
    func getBalanceForWallet(with id: WalletId, callback: @escaping (WalletValue) -> ())

    func sendPayment(from wallet: WalletProtocol, to address: String, value: Decimal, callback: @escaping (Bool) -> ()) throws

    func getTransactions(for wallet: WalletProtocol, callback: @escaping ([WalletTransaction]) -> ())

    func transactionInfo(from data: Data) throws -> WalletTransactionInfo
}


public func CreateWalletService(for type: WalletType, with storage: WalletStorage) -> WalletService {
    switch type {
    case .BTC:
        return BTCService(storage: storage, passphrase: "")
    case .ETH:
        return EthereumService(storage: storage, passphrase: "")
    }
}
