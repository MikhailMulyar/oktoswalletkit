//
// Created by Mikhail Mulyar on 26/01/2018.
//

import Foundation


protocol WalletDataProtocol {
    init?(secureData: SecureData)
    var secureData: SecureData { get }

    var id: WalletId { get }

    var address: WalletAddress { get }
    var privateKey: WalletPrivateKey? { get }
    var publicKey: WalletPublicKey? { get }
    var mnemonic: WalletMnemonic? { get }
}


protocol WalletInternal {
    func walletData() -> WalletDataProtocol

    //Init wallet with custom data representing wallet
    init?(walletData: SecureData)
    //Init wallet with mnemonic string
    init?(mnemonic: String, password: String?, keyPath: WalletKeyPath?)
    //Init wallet with private key data
    init?(privateKey: SecureData)
    //Init wallet with private key string
    init?(privateKey: String)
    //Init wallet with address
    init?(address: SecureData)

    func sign(data: Data) -> Data?
}