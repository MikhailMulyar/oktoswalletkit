//
// Created by Mikhail Mulyar on 29/01/2018.
//

import Foundation
import Sodium


@objc
public class SecureData: SodiumSecureData {

    static func create(from bytes: Bytes) -> SecureData {
        let data = Data(bytes: bytes)
        return create(from: data)
    }
    static func create(from data: Data) -> SecureData {
        let secure = SecureData.readOnlyData(withLength: UInt(data.count), setup: {
            pointer, count in
        })
        secure.write {
            error, secureData in
            guard let s = secureData else { return }
            s.mutableBytes.copyMemory(from: data.bytes, byteCount: data.count)
        }
        try? secure.setProtection(.readOnly)

        return secure
    }
}

extension Array where Element == UInt8 {
    init (_ data: SecureData) {
        let value: Data = Data(bytes: data.bytes, count: data.length)
        self.init(value)
    }
}

extension Data {
    public var secure: SecureData {
        return SecureData.create(from: self)
    }
}
