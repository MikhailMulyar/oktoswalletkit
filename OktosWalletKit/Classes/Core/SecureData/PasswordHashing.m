//
// Created by Mikhail Mulyar on 30/01/2018.
//

#import <OktosWalletKit/PasswordHashing.h>
#import "crypto_box.h"
#import "crypto_pwhash.h"


@implementation NSString (PasswordHash)

- (NSData *) passwordHashWithSalt:(NSData *)salt
{
    return [self argonHashWithSalt:salt
                         ops_limit:crypto_pwhash_OPSLIMIT_INTERACTIVE
                         mem_limit:crypto_pwhash_MEMLIMIT_INTERACTIVE];
}

- (NSData *) argonHashWithSalt:(NSData *)saltData ops_limit:(NSUInteger)ops_limit mem_limit:(NSUInteger)mem_limit
{
    NSParameterAssert(saltData.length == crypto_pwhash_SALTBYTES);
    
    const char *cString = [self cStringUsingEncoding:NSUTF8StringEncoding];
    
    unsigned char salt[crypto_pwhash_SALTBYTES];
    unsigned char key[crypto_box_SEEDBYTES];
    
    [saltData getBytes:salt length:crypto_pwhash_SALTBYTES];
    
    if (crypto_pwhash(key,
                      sizeof key,
                      cString,
                      strlen(cString),
                      salt,
                      ops_limit,
                      mem_limit,
                      crypto_pwhash_ALG_DEFAULT) != 0)
    {
        /* out of memory */
        return nil;
    }
    
    NSData *keyData = [NSData dataWithBytes:key length:(NSUInteger) crypto_box_SEEDBYTES];
    
    return keyData;
}

@end

