//
// Created by Mikhail Mulyar on 30/01/2018.
//

#import <Foundation/Foundation.h>


@interface NSString (PasswordHash)

- (NSData *) passwordHashWithSalt:(NSData *)salt;

@end