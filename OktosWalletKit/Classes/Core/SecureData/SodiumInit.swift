//
// Created by Mikhail Mulyar on 28/01/2018.
//

import Sodium


@objc public class SodiumInit: NSObject {

    @objc public class func setup() {
        _ = Sodium()
    }
}