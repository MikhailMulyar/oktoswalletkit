//
//  SodiumSecureData.h
//

#import <Foundation/Foundation.h>


typedef NS_ENUM (NSInteger, SecureDataProtection) {
    SecureDataProtectionReadWrite = 0, // Default no protection
    SecureDataProtectionReadOnly,
    SecureDataProtectionNoAccess,
};

typedef void (^SecureDataSetup) (void *bytes, NSUInteger length);


/*!
 Secure memory using libsodium.
 */
@interface SodiumSecureData : NSMutableData // Subclassing for convenience

@property (readonly, nonatomic) SecureDataProtection protection;

/*!
 Secure and read only data.
 */
+ (nonnull instancetype) readOnlyDataWithLength:(NSUInteger)length setup:(SecureDataSetup)setupBlock;

/*!
 Secure data has read/write protection in this block. Write data here in case current protection is higher.
 */
- (BOOL) write:(void (^) (NSError *error, SodiumSecureData *secureData))writeBlock;

/*!
 Secure data has readOnly protection in this block. Read data here in case current protection is higher.
 */
- (BOOL) read:(void (^) (NSError *error, SodiumSecureData *secureData))readBlock;

/*!
 Truncate.
 */
- (SodiumSecureData *) truncate:(NSUInteger)length;

/*!
 Set protection.
 @return NO if unable to set protection
 */
- (BOOL) setProtection:(SecureDataProtection)protection error:(NSError **)error;

@end


@interface NSMutableData (SecureData)

- (NSData *) secure_truncate:(NSUInteger)length;

@end
