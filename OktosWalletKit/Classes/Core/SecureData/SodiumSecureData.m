//
//  SodiumSecureData.m
//

#import <OktosWalletKit/SodiumSecureData.h>
#import <OktosWalletKit/OktosWalletKit-Swift.h>
#import "utils.h"


@interface SodiumSecureData ()
@property void       *secureBytes;
@property NSUInteger secureLength;
@end


@implementation SodiumSecureData

+ (void) initialize
{
    [SodiumInit setup];
}

- (instancetype) initWithLength:(NSUInteger)length
{
    if ((self = [super init]))
    {
        [SodiumInit setup]; // It's already init'ed, but just to be safe
        _secureLength = length;
        _secureBytes  = sodium_malloc(length);
    }
    return self;
}

+ (instancetype) readOnlyDataWithLength:(NSUInteger)length setup:(SecureDataSetup)setupBlock
{
    SodiumSecureData *secureData = [[SodiumSecureData alloc] initWithLength:length];
    setupBlock(secureData.secureBytes, secureData.length);
    if (![secureData setProtection:SecureDataProtectionReadOnly error:nil])
    {
        return nil;
    }
    return secureData;
}

- (void) dealloc
{
    sodium_free(_secureBytes);
}

- (BOOL) setProtection:(SecureDataProtection)protection error:(NSError **)error
{
    int result = INT_MIN;
    switch (protection)
    {
        case SecureDataProtectionReadWrite:result = sodium_mprotect_readwrite(_secureBytes);
            break;
        case SecureDataProtectionReadOnly:result = sodium_mprotect_readonly(_secureBytes);
            break;
        case SecureDataProtectionNoAccess:result = sodium_mprotect_noaccess(_secureBytes);
            break;
        default:return NO;
    }
    if (result != 0)
    {
        if (error)
            * error = [NSError errorWithDomain:@"sodium.error.domain"
                                          code:1
                                      userInfo:@{NSLocalizedDescriptionKey: [NSString stringWithFormat:@"Unable to set protection (%@)", @(result)]}];
        return NO;
    }
    return YES;
}

- (NSUInteger) length
{
    return _secureLength;
}

- (const void *) bytes
{
    return _secureBytes;
}

- (void *) mutableBytes
{
    return _secureBytes;
}

- (BOOL) write:(void (^) (NSError *error, SodiumSecureData *secureData))writeBlock
{
    SecureDataProtection protection = self.protection;
    NSError              *error     = nil;
    [self setProtection:SecureDataProtectionReadWrite error:& error];
    writeBlock(error, self);
    return [self setProtection:protection error:& error];
}

- (BOOL) read:(void (^) (NSError *error, SodiumSecureData *secureData))readBlock
{
    SecureDataProtection protection = self.protection;
    NSError              *error     = nil;
    [self setProtection:SecureDataProtectionReadOnly error:& error];
    readBlock(error, self);
    return [self setProtection:protection error:& error];
}

- (SodiumSecureData *) truncate:(NSUInteger)length
{
    if (length == 0) return self;
    return [SodiumSecureData readOnlyDataWithLength:(self.length - length) setup:^(void *bytes, NSUInteger length)
    {
        memcpy(bytes, self.bytes, length);
    }];
}

- (NSData *) secure_truncate:(NSUInteger)length
{
    return [self truncate:length];
}

@end


@implementation NSMutableData (SecureData)

- (NSData *) secure_truncate:(NSUInteger)length
{
    if (length == 0) return self;
    return [NSData dataWithBytes:self.bytes length:self.length - length];
}

@end
