//
// Created by Mikhail Mulyar on 26/01/2018.
//

import Foundation
import CKMnemonic


struct GeneralWalletValue: WalletValue {
    let userRepresentation: String
    let value: Decimal
}


struct GeneralWalletMnemonic: WalletMnemonic {
    let userRepresentation: String
    let internalData: SecureData?

    var data: SecureData? {
        return self.internalData.flatMap { SecureData.create(from: $0 as Data) }
    }
}


struct GeneralWalletId: WalletId {
    let id: String
    let type: WalletType
}


struct GeneralWalletKey: WalletPrivateKey, WalletPublicKey {
    let userRepresentation: String
    let internalData: SecureData

    var data: SecureData {
        return SecureData.create(from: self.internalData as Data)
    }
}


struct GeneralWalletAddress: WalletAddress {
    let userRepresentation: String
    let internalData: SecureData

    var data: SecureData {
        return SecureData.create(from: self.internalData as Data)
    }
}


struct GeneralWalletTransaction: WalletTransaction {
    let to: WalletAddress
    let from: WalletAddress
    let value: WalletValue
}


extension WalletType {

    func walletType() -> WalletInternal.Type {
        switch self {
        case .BTC: return BTCWallet.self
        case .ETH: return EthereumWallet.self
        }
    }
}


class GeneralWallet: WalletProtocol {

    let wallet: WalletInternal

    var data: SecureData {
        return SecureData.create(from: self.wallet.walletData().secureData as Data)
    }

    required init?(type: WalletType, mnemonic: String, password: String? = nil, keyPath: WalletKeyPath? = nil) {
        guard let wallet = type.walletType().init(mnemonic: mnemonic, password: password, keyPath: keyPath) else { return nil }
        self.wallet = wallet
    }

    required init?(type: WalletType, walletData: SecureData) {
        guard let wallet = type.walletType().init(walletData: walletData) else { return nil }
        self.wallet = wallet
    }

    required init?(type: WalletType, privateKey: SecureData) {
        guard let wallet = type.walletType().init(privateKey: privateKey) else { return nil }
        self.wallet = wallet
    }

    required init?(type: WalletType, privateKey: String) {
        guard let wallet = type.walletType().init(privateKey: privateKey) else { return nil }
        self.wallet = wallet
    }

    required init?(type: WalletType, address: SecureData) {
        guard let wallet = type.walletType().init(address: address) else { return nil }
        self.wallet = wallet
    }

    var id: WalletId {
        return self.wallet.walletData().id
    }
    var privateKey: WalletPrivateKey? {
        return self.wallet.walletData().privateKey
    }
    var publicKey: WalletPublicKey? {
        return self.wallet.walletData().publicKey
    }
    var address: WalletAddress {
        return self.wallet.walletData().address
    }
    var mnemonic: WalletMnemonic? {
        return self.wallet.walletData().mnemonic
    }

    func sign(data: Data) -> Data? {
        return self.wallet.sign(data: data)
    }
}


struct GeneralWalletInfo {
    let address: SecureData
    let privateKey: SecureData?
    let mnemonic: SecureData?


    enum CodingKeys: String, CodingKey {
        case address
        case privateKey
        case mnemonic
    }
}


extension GeneralWalletInfo: Codable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        address = SecureData.create(from: try values.decode(Data.self, forKey: .address))
        privateKey = try values.decode(Data?.self, forKey: .privateKey).flatMap { SecureData.create(from: $0) }
        mnemonic = try values.decode(Data?.self, forKey: .mnemonic).flatMap { SecureData.create(from: $0) }
    }

    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(address as Data, forKey: .address)
        try container.encode(privateKey as Data?, forKey: .privateKey)
        try container.encode(mnemonic as Data?, forKey: .mnemonic)
    }
}
