//
// Created by Mikhail Mulyar on 29/01/2018.
//

import Foundation
import ethers
import CKMnemonic
import CoreBitcoin
import CryptoSwift


class EthereumWallet: WalletInternal {
    var data: WalletDataProtocol

    required init?(mnemonic: String, password: String?, keyPath: WalletKeyPath?) {
        guard let mnemonicObject = BTCMnemonic.init(words: mnemonic.components(separatedBy: " "), password: password, wordListType: .english),
              let key = mnemonicObject.keychain.key(withPath: (keyPath ?? WalletKeyPath.Ethereum).path),
              let account = Account(privateKey: key.privateKey as Data),
              let info = try? EthereumInfo(genericInfo: GeneralWalletInfo(address: account.address.data.secure,
                                                                          privateKey: (key.privateKey as Data).secure,
                                                                          mnemonic: mnemonicObject.entropy.secure)) else {
            return nil
        }

        self.data = info
    }

    required init?(walletData: SecureData) {
        guard let info = EthereumInfo(secureData: walletData) else { return nil }
        self.data = info
    }

    required init?(privateKey: SecureData) {
        guard let account = Account(privateKey: privateKey as Data), let info = try? EthereumInfo(account: account) else { return nil }
        self.data = info
    }

    required init?(privateKey: String) {
        guard let account = Account(privateKey: Data(hex: privateKey)), let info = try? EthereumInfo(account: account) else { return nil }
        self.data = info
    }

    required init?(address: SecureData) {
        guard let address = Address(data: address as Data) else { return nil }

        let info = GeneralWalletInfo(address: address.data.secure, privateKey: nil, mnemonic: nil)
        guard let etherInfo = try? EthereumInfo(genericInfo: info) else { return nil }
        self.data = etherInfo
    }

    func walletData() -> WalletDataProtocol {
        return self.data
    }

    func sign(data: Data) -> Data? {
        guard let privateKey = self.walletData().privateKey,
              let account = Account(privateKey: privateKey.data as Data) else { return nil }

        let t: Transaction? = Transaction(data: data)
        guard let transaction = t else { return nil }

        account.sign(transaction)

        guard let _ = transaction.signature else { return nil }

        return transaction.serialize()
    }
}


struct EthereumInfo: WalletDataProtocol {

    let secureData: SecureData
    let genericInfo: GeneralWalletInfo

    fileprivate init(account: Account) throws {
        let mnemonic: Data? = account.mnemonicData
        self.genericInfo = GeneralWalletInfo(address: account.address.data.secure,
                                             privateKey: account.privateKey.secure,
                                             mnemonic: mnemonic.flatMap { $0.secure })

        self.secureData = SecureData.create(from: try JSONEncoder().encode(self.genericInfo))
    }

    init(genericInfo: GeneralWalletInfo) throws {
        self.genericInfo = genericInfo
        self.secureData = SecureData.create(from: try JSONEncoder().encode(self.genericInfo))
    }

    init?(secureData: SecureData) {
        guard let info = try? JSONDecoder().decode(GeneralWalletInfo.self, from: secureData as Data) else { return nil }

        self.secureData = secureData
        self.genericInfo = info
    }

    var id: WalletId {
        let address = Address(data: self.genericInfo.address as Data)
        return GeneralWalletId(id: address!.icapAddress, type: EthereumService.walletType)
    }
    var privateKey: WalletPrivateKey? {
        return self.genericInfo.privateKey.flatMap { GeneralWalletKey(userRepresentation: ($0 as Data).toHexString(), internalData: $0) }
    }
    var publicKey: WalletPublicKey? {
        return nil
    }
    var address: WalletAddress {
        let address = Address(data: self.genericInfo.address as Data)
        return GeneralWalletAddress(userRepresentation: "\(address?.checksumAddress.lowercased() ?? "")", internalData: self.genericInfo.address)
    }
    var mnemonic: WalletMnemonic? {
        return self.genericInfo.mnemonic.flatMap {
            guard let mnemonic = try? CKMnemonic.mnemonicString(from: ($0 as Data).toHexString(), language: .english) else { return nil }
            return GeneralWalletMnemonic(userRepresentation: mnemonic, internalData: $0)
        }
    }
}