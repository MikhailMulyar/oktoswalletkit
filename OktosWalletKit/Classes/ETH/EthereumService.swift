//
// Created by Mikhail Mulyar on 22/01/2018.
//

import Foundation
import CryptoSwift
import ethers
import Sodium


let WeiInETH = Decimal(sign: .plus, exponent: 18, significand: 1)


class EthereumService: WalletService, WalletServiceInternal {

    static var walletType: WalletType {
        return .ETH
    }

    let storage: WalletStorage
    let passphraseHash: SecureData

    let sodium: Sodium = Sodium()

    lazy var provider: Provider = EtherscanProvider(chainId: ChainId.ChainIdRinkeby, apiKey: "5Z553MKCV35Y73ANP1C9NCEFMCD39GXZM1")

    var price: Float?

    required init(storage: WalletStorage, passphrase: String) {
        self.storage = storage
        self.passphraseHash = SecureData.create(from: EthereumService.calculatePassphraseHash(passphrase))
        //        guard let account = Account(mnemonicPhrase: "horn purity agent math syrup six object begin certain replace member main") else { return }
        //        guard let account = Account(privateKey: ethers.SecureData.hexString(toData: "0x162008b4e2e4b2b22a9c0c414f136c8e2ab902d149ca872467804122376edf05")) else { return }


        //        guard let pricePromise = self.provider.getEtherPrice() else { return }
        //        pricePromise.onCompletion {
        //            promise in
        //
        //            if promise?.result == nil || promise?.error != nil {
        //                return
        //            }
        //
        //            self.price = promise?.value
        //        }

        //        let wallet = self.importWallet(mnemonic: account.mnemonicPhrase)
        //
        //        print(wallet?.address.userRepresentation)
        //        print(wallet?.mnemonic?.userRepresentation)
    }

    func createNewWalletData() -> SecureData {
        let account = Account.randomMnemonic()!

        let mnemonic: Data? = account.mnemonicData
        let info = GeneralWalletInfo(address: account.address.data.secure,
                                     privateKey: account.privateKey.secure,
                                     mnemonic: mnemonic.flatMap { $0.secure })

        let secure = SecureData.create(from: try! JSONEncoder().encode(info))

        return secure
    }

    func getBalance(for wallet: WalletProtocol, callback: @escaping  (WalletValue) -> ()) {
        guard let promise = self.provider.getBalance(Address(data: wallet.address.data as Data)) else { return }
        promise.onCompletion {
            [weak self] promise in

            if promise?.result == nil || promise?.error != nil {
                return
            }
            guard let value = promise?.value else { return }
            let formatted = Payment.formatEther(value, options: EtherFormatOption.commify.rawValue | EtherFormatOption.approximate.rawValue)
            var converted: String = ""
            if let price = self?.price,
               let dollars = value.mul(BigNumber(integer: Int(price))),
               let formatted = Payment.formatEther(dollars, options: EtherFormatOption.commify.rawValue | EtherFormatOption.approximate.rawValue) {
                converted = "  $\(formatted)"
            }
            callback(GeneralWalletValue(userRepresentation: "Ξ \(formatted ?? "0")" + converted,
                                        value: Decimal(string: "\(promise?.value.decimalString ?? "0")") ?? 0))
        }
    }

    func getBalanceForWallet(with id: WalletId, callback: @escaping (WalletValue) -> ()) {
        guard let wallet = self.wallet(for: id) else { return }
        self.getBalance(for: wallet, callback: callback)
    }

    func sendPayment(from wallet: WalletProtocol, to address: String, value: Decimal, callback: @escaping (Bool) -> ()) throws {
        guard let privateKey = wallet.privateKey?.data else { throw WalletServiceError.noPrivateKey }

        guard let address = Address(string: address), let account = Account(privateKey: privateKey as Data) else {
            throw WalletServiceError.incorrectPrivateKey
        }

        let transaction = Transaction()
        transaction.toAddress = address
        transaction.value = BigNumber(number: (WeiInETH * value) as NSNumber)
        transaction.gasPrice = BigNumber(decimalString: "25000000000")
        transaction.gasLimit = BigNumber(integer: 21_000)

        guard let noncePromise = self.provider.getTransactionCount(account.address) else {
            callback(false)
            return
        }

        noncePromise.onCompletion {
            [weak self] noncePromise in

            guard let count = noncePromise?.value else {
                callback(false)
                return
            }

            transaction.nonce = UInt(count)

            account.sign(transaction)

            let signed = transaction.serialize()

            guard let promise = self?.provider.sendTransaction(signed) else {
                callback(false)
                return
            }

            promise.onCompletion {
                promise in

                if let error = promise?.error {
                    print(error)
                    callback(false)
                    return
                }

                callback(true)
            }
        }
    }

    func getTransactions(for wallet: WalletProtocol, callback: @escaping ([WalletTransaction]) -> ()) {
        guard let promise = self.provider.getTransactions(Address(data: wallet.address.data as Data), startBlockTag: 0) else { return }

        promise.onCompletion {
            [weak self] promise in

            if promise?.result == nil || promise?.error != nil {
                return
            }
            guard let value = promise?.value as? [TransactionInfo] else { return }

            let transactions = value.map {
                info -> WalletTransaction in
                let to = GeneralWalletAddress(userRepresentation: "\(info.toAddress.checksumAddress ?? "")", internalData: info.toAddress.data.secure)
                let from = GeneralWalletAddress(userRepresentation: "\(info.fromAddress.checksumAddress ?? "")",
                                                internalData: info.fromAddress.data.secure)

                let formatted = Payment.formatEther(info.value, options: EtherFormatOption.commify.rawValue | EtherFormatOption.approximate.rawValue)
                var converted: String = ""
                if let price = self?.price,
                   let dollars = info.value.mul(BigNumber(integer: Int(price))),
                   let formatted = Payment.formatEther(dollars,
                                                       options: EtherFormatOption.commify.rawValue | EtherFormatOption.approximate.rawValue) {
                    converted = "  $\(formatted)"
                }
                let value = GeneralWalletValue(userRepresentation: "Ξ \(formatted ?? "0")" + converted,
                                               value: Decimal(string: "\(info.value.decimalString ?? "0")") ?? 0)
                return GeneralWalletTransaction(to: to, from: from, value: value)
            }

            callback(transactions)
        }
    }

    func transactionInfo(from data: Data) throws -> WalletTransactionInfo {
        let t: Transaction? = Transaction(data: data)
        guard let transaction = t else { throw WalletServiceError.failedToDecodeTransaction }

        let fee = Decimal(string: transaction.gasPrice.decimalString) ?? 0
        let amount = Decimal(string: transaction.value.decimalString) ?? 0
        let outputs = [(transaction.toAddress?.checksumAddress ?? ""): GeneralWalletValue(userRepresentation: "\(amount)", value: amount)]
        return WalletTransactionInfo(outputs: outputs, fee: GeneralWalletValue(userRepresentation: "\(fee)", value: fee))
    }


    func isValidMnemonic(_ phrase: String) -> Bool {
        return Account.isValidMnemonicPhrase(phrase)
    }

    func isValidMnemonic(word: String) -> Bool {
        return Account.isValidMnemonicWord(word)
    }

    func isValidPrivateKey(_ key: String) -> Bool {
        let data = Data(hex: key)

        guard let acc = Account(privateKey: data), let _ = acc.address else {
            return false
        }

        return true
    }

    func isValidPrivateKey(_ key: SecureData) -> Bool {
        let maxCurve = Data(hex: "fffffffffffffffffffffffffffffffebaaedce6af48a03bbfd25e8cd0364141")

        guard let curveNumber = BigNumber(data: maxCurve),
              let keyNumber = BigNumber(data: key as Data),
              curveNumber.greaterThan(keyNumber) else {
            return false
        }
        guard let acc = Account(privateKey: key as Data), let _ = acc.address else {
            return false
        }

        return true
    }

    func isValidAddress(_ address: String) -> Bool {
        guard let _ = Address(string: address) else {
            return false
        }
        return true
    }
}
