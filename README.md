# OktosWalletKit

[![CI Status](http://img.shields.io/travis/mikhailmulyar/OktosWalletKit.svg?style=flat)](https://travis-ci.org/mikhailmulyar/OktosWalletKit)
[![Version](https://img.shields.io/cocoapods/v/OktosWalletKit.svg?style=flat)](http://cocoapods.org/pods/OktosWalletKit)
[![License](https://img.shields.io/cocoapods/l/OktosWalletKit.svg?style=flat)](http://cocoapods.org/pods/OktosWalletKit)
[![Platform](https://img.shields.io/cocoapods/p/OktosWalletKit.svg?style=flat)](http://cocoapods.org/pods/OktosWalletKit)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

OktosWalletKit is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'OktosWalletKit'
```

## Author

mikhailmulyar, mulyarm@gmail.com

## License

OktosWalletKit is available under the MIT license. See the LICENSE file for more info.
