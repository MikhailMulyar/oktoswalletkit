//
// Created by Mikhail Mulyar on 27/01/2018.
// Copyright (c) 2018 CocoaPods. All rights reserved.
//

import Foundation
import OktosWalletKit


class FakeWalletStorage: WalletStorage {
    func wallets(of type: WalletType) -> [SecureData] {
        return []
    }

    func storeWalletData(_ data: SecureData, for id: WalletId) {
    }

    func removeWalletData(for id: WalletId) {
    }

    func getWalletData(for id: WalletId) -> SecureData? {
        return nil
    }
}
