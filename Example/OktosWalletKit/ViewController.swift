//
//  ViewController.swift
//  OktosWalletKit
//
//  Created by mikhailmulyar on 01/19/2018.
//  Copyright (c) 2018 mikhailmulyar. All rights reserved.
//

import UIKit
import OktosWalletKit


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let walletService = CreateWalletService(for: .BTC, with: FakeWalletStorage())

        let wallet = walletService.createNewWallet()

        NSLog("Wallet: %@", wallet.address.userRepresentation)

        walletService.getBalance(for: wallet) {
            (balance) in

        }

        try? walletService.sendPayment(from: wallet, to: "1Jr2uxvkUfJ5hw7NL6SUwEhXoptFn9nXSw", value: 0.1) {
            (result) in

        }

        do {
            let newWallet = try walletService.importWallet(address: wallet.address.data)
            if newWallet.id.id == wallet.id.id {
                print("success")
            }
        } catch let error {
            print("error")
        }
    }
}

